import logging
from unittest.mock import MagicMock

import flywheel
import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_deid_export.parser import (
    create_key_dict,
    get_analysis_parent,
    get_keys_from_path,
    lookup_project,
    parse_args_from_context,
)


@pytest.fixture
def mock_lookup_project(mocker):
    mock_lookup = mocker.patch("fw_gear_deid_export.parser.lookup_project")
    return mock_lookup


@pytest.fixture
def mock_get_analysis_parent(mocker):
    mock_get = mocker.patch("fw_gear_deid_export.parser.get_analysis_parent")
    return mock_get


@pytest.fixture
def mock_user_has_perms(mocker):
    mock_has_perms = mocker.patch(
        "fw_gear_deid_export.parser.user_has_project_permissions"
    )
    return mock_has_perms


@pytest.fixture
def mock_create_jinja_var_df(mocker):
    mock_create = mocker.patch("fw_gear_deid_export.parser.create_jinja_var_df")
    return mock_create


@pytest.fixture
def mock_create_key_dict(mocker):
    mock_create = mocker.patch("fw_gear_deid_export.parser.create_key_dict")
    return mock_create


@pytest.fixture
def mock_get_keys(mocker):
    mock_get = mocker.patch("fw_gear_deid_export.parser.get_keys_from_path")
    return mock_get


def test_parse_args_from_context(
    mock_lookup_project,
    mock_get_analysis_parent,
    mock_user_has_perms,
    mock_create_jinja_var_df,
    mock_create_key_dict,
):
    config_args = {"project_path": "test/test_project", "overwrite_files": "Skip"}
    mock_lookup_project.return_value = MagicMock(spec=flywheel.Project)

    context = MagicMock(spec=GearToolkitContext)
    context.config = config_args
    context.destination = {"id": "0000"}
    context.output_dir = "output"

    mock_project = MagicMock(spec=flywheel.Project)
    mock_project.id = "1234"
    mock_lookup_project.return_value = mock_project

    analysis_parent = MagicMock(spec=flywheel.Project)
    analysis_parent.id = "5678"
    mock_get_analysis_parent.return_value = analysis_parent

    mock_user_has_perms.return_value = True
    context.get_input_path.side_effect = ["path/to/profile.yaml", "path/to/subject.csv"]

    res = parse_args_from_context(context)
    assert res.get("dest_proj_id") == "1234"
    assert res.get("container_id") == "5678"
    assert res.get("csv_output_path") == "output/5678_export.csv"
    assert res.get("template_path") == "path/to/profile.yaml"
    assert res.get("overwrite") == "Skip"

    mock_lookup_project.assert_called_once()
    mock_get_analysis_parent.assert_called_once()
    mock_user_has_perms.assert_called_once()
    mock_create_jinja_var_df.assert_called_once()
    mock_create_key_dict.assert_called_once()


def test_parse_args_from_context_no_project(mock_lookup_project, caplog):
    context = MagicMock(spec=GearToolkitContext)
    context.config = {"project_path": "test/bad_path"}
    mock_lookup_project.return_value = None

    with pytest.raises(SystemExit):
        parse_args_from_context(context)

    assert "Project test/bad_path not found." in caplog.text


def test_parse_args_from_context_no_perms(
    mock_lookup_project, mock_get_analysis_parent, mock_user_has_perms, caplog
):
    mock_lookup_project.return_value = MagicMock(spec=flywheel.Project)

    context = MagicMock(spec=GearToolkitContext)
    context.config = {"project_path": "test/test_project"}
    context.destination = {"id": "0000"}
    context.output_dir = "output"

    mock_project = MagicMock(spec=flywheel.Project)
    mock_project.id = "1234"
    mock_lookup_project.return_value = mock_project

    analysis_parent = MagicMock(spec=flywheel.Project)
    analysis_parent.id = "5678"
    mock_get_analysis_parent.return_value = analysis_parent

    mock_user_has_perms.return_value = False

    res = parse_args_from_context(context)

    assert not res
    assert "Please add permission for this user" in caplog.text


def test_lookup_project():
    project_resolver_path = "test/project"
    fw_client = MagicMock(spec=flywheel.Client)
    project = MagicMock(spec=flywheel.Project)
    project.container_type = "project"
    fw_client.lookup.return_value = project

    res = lookup_project(fw_client, project_resolver_path)
    assert res == project


def test_lookup_project_not_a_project(caplog):
    project_resolver_path = "test/project/subject"
    fw_client = MagicMock(spec=flywheel.Client)
    not_project = MagicMock(spec=flywheel.Subject)
    not_project.container_type = "not a project"
    fw_client.lookup.return_value = not_project

    res = lookup_project(fw_client, project_resolver_path)
    assert not res
    assert "not a project" in caplog.text


def test_lookup_project_fail(caplog):
    project_resolver_path = "test/bad_project"
    fw_client = MagicMock(spec=flywheel.Client)
    fw_client.lookup.side_effect = flywheel.ApiException

    res = lookup_project(fw_client, project_resolver_path)
    assert not res
    assert "could not retrieve a project at test/bad_project" in caplog.text


def test_get_analysis_parent(caplog):
    caplog.set_level(logging.INFO)
    container_id = "1234"
    fw_client = MagicMock(spec=flywheel.Client)
    subject = MagicMock(spec=flywheel.Subject)
    subject.container_type = "subject"
    subject.id = "fake subject id"
    session = MagicMock(spec=flywheel.Session)
    session.id = "fake session id"
    session.parent = subject
    fw_client.get.side_effect = [session, subject]

    res = get_analysis_parent(fw_client, container_id)

    assert res == subject
    assert "Destination analysis fake session id parent" in caplog.text


def test_create_key_dict(mock_get_keys, tmp_path):
    secret_key_path = tmp_path / "secret_key.txt"
    secret_key_path.write_text("this is a secret key", encoding="utf-8")

    mock_get_keys.side_effect = ["public_key.pem", "private_key.pem", secret_key_path]

    context = MagicMock(spec=GearToolkitContext)
    context.config = {
        "public_key": "test/project/files/public_key.pem",
        "private_key": "test/project/files/private_key.pem",
        "secret_key": "test/project/files/secret_key.txt",
    }

    res = create_key_dict(context)
    assert res.get("PUBLIC_KEY") == "public_key.pem"
    assert res.get("PRIVATE_KEY") == "private_key.pem"
    assert res.get("SECRET_KEY") == "this is a secret key"


def test_get_keys_from_path():
    fw_client = MagicMock()  # spec = GearToolkitContext fails at .lookup()
    key_file = MagicMock(spec=flywheel.FileOutput)
    key_file.name = "secret_key.txt"
    key_file.file_id = "fake file id"
    downloaded_key_file = MagicMock()
    key_file.download.return_value = downloaded_key_file
    fw_client.lookup.return_value = key_file
    key_input = "test/project/files/secret_key.txt"
    key_type = "secret"
    workdir = "workdir"

    res = get_keys_from_path(fw_client, key_input, key_type, workdir)
    assert res == "workdir/secret_key.txt"
