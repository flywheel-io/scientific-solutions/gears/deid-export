import os
import shutil
import tempfile
from dataclasses import dataclass
from pathlib import Path
from uuid import uuid4

import flywheel
import pytest

DATA_ROOT = Path(__file__).parent / "data"


@dataclass
class Parents(dict):
    project: str
    subject: str
    session: str
    acquisition: str

    def __getitem__(self, key):
        return getattr(self, key)


@pytest.fixture(scope="function")
def data_file():
    def get_data_file(filename):
        fd, path = tempfile.mkstemp(suffix=Path(filename).suffix)
        os.close(fd)
        src_path = os.path.join(DATA_ROOT, filename)
        shutil.copy(src_path, path)

        return path

    return get_data_file


class MockFinder:
    def __init__(self, arr):
        self.arr = arr

    def iter(self):
        for x in self.arr:
            yield x

    def __len__(self):
        return len(self.arr)

    def __call__(self):
        return self.arr


class MockContainerMixin:
    analyses = []
    files = []
    _update = None  # to store updates made to the container when calling `update`

    def reload(self):
        if self._update:
            for k, v in self._update.items():
                setattr(self, k, v)
            self._update = None
        return self

    def update(self, *args, **kwargs):
        self._update = flywheel.util.params_to_dict("update", args, kwargs)


class MockAcquisition(MockContainerMixin, flywheel.Acquisition):
    pass


class MockSession(MockContainerMixin, flywheel.Session):
    acquisitions = MockFinder([])


class MockSubject(MockContainerMixin, flywheel.Subject):
    sessions = MockFinder([])


class MockProject(MockContainerMixin, flywheel.Project):
    subjects = MockFinder([])


@pytest.fixture(scope="function")
def fw_project():
    """Mock a flywheel project"""

    def get_fw_project(n_subjects=5, n_sessions=1, n_acquisitions=1, n_files=1):
        project = MockProject(label="test", info={"study_id": "test"})
        project_id = str(uuid4())
        subjects = []
        for i in range(n_subjects):
            subject = MockSubject(label=f"sub-{i}", info={"site_num": str(i)})
            subject_id = str(uuid4())
            sessions = []
            for j in range(n_sessions):
                session = MockSession(label=f"ses-{j}", info={"this": "is"})
                session_id = str(uuid4())
                acquisitions = []
                for k in range(n_acquisitions):
                    acquisition = MockAcquisition(
                        label=f"acq-{k}", info={"some": "rather"}
                    )
                    acquisition_id = str(uuid4())
                    files = []
                    for n in range(n_files):
                        files.append(
                            flywheel.FileEntry(
                                name=f"file-{n}", info={"useless": "info"}
                            )
                        )
                    acquisition.files = files
                    acquisition.parents = Parents(
                        project=project_id,
                        subject=subject_id,
                        session=session_id,
                        acquisition=acquisition_id,
                    )
                    acquisitions.append(acquisition)
                session.acquisitions = MockFinder(acquisitions)
                session.parents = Parents(
                    project=project_id,
                    subject=subject_id,
                    session=session_id,
                    acquisition="",
                )
                sessions.append(session)
            subject.sessions = MockFinder(sessions)
            subject.parents = Parents(
                project=project_id, subject=subject_id, session="", acquisition=""
            )
            subjects.append(subject)
        project.subjects = MockFinder(subjects)
        project.parents = Parents(
            project=project_id, subject="", session="", acquisition=""
        )
        return project

    return get_fw_project
