import tempfile
from pathlib import Path

from ruamel.yaml import YAML

from fw_gear_deid_export.deid_template import update_deid_profile

DATA_ROOT = Path(__file__).parent / "data"


def test_can_update_deid_dicom_profile():
    with tempfile.NamedTemporaryFile(suffix=".yaml") as tmpfile:
        replace_with = {
            "DATE_INCREMENT": -20,
            "SUBJECT_ID": "TEST",
            "PATIENT_BD_BOOL": False,
        }

        update_deid_profile(
            DATA_ROOT / "example4-deid-profile-jinja.yaml",
            updates=replace_with,
            dest_path=tmpfile.name,
        )

        with open(tmpfile.name, "r") as fid:
            yaml = YAML(typ="rt")
            new_config = yaml.load(fid)

        assert new_config["only-config-profiles"] is True
        assert new_config["zip"]["validate-zip-members"] is True
        assert new_config["dicom"]["date-increment"] == -20
        assert new_config["dicom"]["fields"][0]["remove"] is False
        assert new_config["dicom"]["fields"][1]["replace-with"] == "TEST"
        assert new_config["flywheel"]["subject"]["fields"][0]["replace-with"] == "TEST"


def test_can_update_deid_dicom_profile_filename_section():
    updates = {"SUBJECT_ID": "TEST", "INCREMENT_DATE": False}
    with tempfile.NamedTemporaryFile(suffix=".yaml") as tmpfile:
        update_file = update_deid_profile(
            DATA_ROOT / "example4-deid-profile-jinja.yaml",
            updates=updates,
            dest_path=tmpfile.name,
        )
        with open(update_file, "r") as fid:
            yaml = YAML(typ="rt")
            template = yaml.load(fid)
            assert template["dicom"]["filenames"][0]["output"] == "TEST.dcm"
            assert template["dicom"]["filenames"][1]["output"] == "TEST_{reg_date}.dcm"
            assert (
                template["dicom"]["filenames"][1]["groups"][0]["increment-date"]
                is False
            )
