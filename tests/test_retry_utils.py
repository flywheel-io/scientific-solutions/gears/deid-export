import logging
from unittest import mock
from unittest.mock import MagicMock

import pytest
from flywheel import AcquisitionOutput, FileSpec, SubjectOutput

from fw_gear_deid_export.retry_utils import (
    create_container_with_retry,
    quote_numeric_string,
    upload_file_with_retry,
)


def test_upload_file_success(caplog):
    acquisition = MagicMock(spec=AcquisitionOutput)
    file = MagicMock(spec=FileSpec)
    fw_client = MagicMock()
    upload_fn = fw_client.upload_file_to_acquisition

    upload_file_with_retry(acquisition, upload_fn, file)

    upload_fn.assert_called_once()
    assert "Encountered error uploading file" not in caplog.text


def test_upload_file_one_retry(caplog):
    acquisition = MagicMock(spec=AcquisitionOutput)
    file = MagicMock(spec=FileSpec)
    file.name = "test.dcm"
    fw_client = MagicMock()
    upload_fn = fw_client.upload_file_to_acquisition
    upload_fn.side_effect = [Exception("test"), "success"]
    acquisition.reload.return_value = acquisition

    upload_file_with_retry(acquisition, upload_fn, file)

    assert "Encountered error uploading file" in caplog.text
    assert upload_fn.call_count == 2


def test_upload_file_retries_exceeded(caplog):
    acquisition = MagicMock(spec=AcquisitionOutput)
    file = MagicMock(spec=FileSpec)
    file.name = "test.dcm"
    fw_client = MagicMock()
    upload_fn = fw_client.upload_file_to_acquisition
    upload_fn.side_effect = Exception("test")
    acquisition.reload.return_value = acquisition

    with mock.patch("time.sleep"), pytest.raises(Exception):
        upload_file_with_retry(acquisition, upload_fn, file)

    assert "Retries remaining: 0" in caplog.text
    assert upload_fn.call_count == 4


def test_upload_file_replace(caplog):
    caplog.set_level(logging.INFO)
    acquisition = MagicMock(spec=AcquisitionOutput)
    existing_file = MagicMock(spec=FileSpec)
    existing_file.name = "test.dcm"
    existing_file.version = 1
    acquisition.files = [existing_file]
    file = MagicMock(spec=FileSpec)
    file.name = "test.dcm"
    acquisition_after_reload = MagicMock(spec=AcquisitionOutput)
    updated_file = MagicMock(spec=FileSpec)
    updated_file.name = "test.dcm"
    updated_file.version = 2
    acquisition_after_reload.files = [updated_file]
    acquisition_after_reload.type = "acquisition"
    acquisition_after_reload.label = "test"
    acquisition.reload.return_value = acquisition_after_reload
    fw_client = MagicMock()
    upload_fn = fw_client.upload_file_to_acquisition
    upload_fn.side_effect = Exception("test")

    upload_file_with_retry(acquisition, upload_fn, file)

    assert "Encountered error uploading file" in caplog.text
    assert "Upload completed with above error" in caplog.text
    upload_fn.assert_called_once()


def test_create_container_success(caplog):
    dest_parent = MagicMock(spec=SubjectOutput)
    dest_parent.sessions = MagicMock()
    origin_type = "session"
    label = "created_session"
    meta_dict = {}

    create_container_with_retry(dest_parent, origin_type, label, meta_dict)
    dest_parent.add_session.assert_called_once()
    assert "Encountered error creating session created_session" not in caplog.text


def test_create_container_one_retry(caplog):
    dest_parent = MagicMock(spec=SubjectOutput)
    dest_parent.sessions = MagicMock()
    origin_type = "session"
    label = "created_session"
    meta_dict = {}
    dest_parent.add_session.side_effect = [Exception("test"), "success"]
    dest_parent.reload.return_value = dest_parent
    dest_parent.sessions.find_first.return_value = None

    create_container_with_retry(dest_parent, origin_type, label, meta_dict)
    assert dest_parent.add_session.call_count == 2
    assert "Encountered error creating session created_session" in caplog.text


def test_create_container_retries_exceeded(caplog):
    dest_parent = MagicMock(spec=SubjectOutput)
    dest_parent.sessions = MagicMock()
    origin_type = "session"
    label = "created_session"
    meta_dict = {}
    dest_parent.add_session.side_effect = Exception("test")
    dest_parent.reload.return_value = dest_parent
    dest_parent.sessions.find_first.return_value = None

    with mock.patch("time.sleep"), pytest.raises(Exception):
        create_container_with_retry(dest_parent, origin_type, label, meta_dict)

    assert dest_parent.add_session.call_count == 4
    assert "Retries remaining: 0" in caplog.text


def test_create_container_success_with_error(caplog):
    caplog.set_level(logging.INFO)
    dest_parent = MagicMock(spec=SubjectOutput)
    dest_parent.sessions = MagicMock()
    origin_type = "session"
    label = "created_session"
    meta_dict = {}
    dest_parent.add_session.side_effect = Exception("test")
    dest_parent.reload.return_value = dest_parent
    dest_parent.sessions.find_first.return_value = MagicMock()

    create_container_with_retry(dest_parent, origin_type, label, meta_dict)

    dest_parent.add_session.assert_called_once()
    assert "Encountered error" in caplog.text
    assert "Container creation completed" in caplog.text


def test_quote_numeric_string():
    in_str = "test"
    out_str = quote_numeric_string(in_str)
    assert out_str == "test"

    in_str = "1"
    out_str = quote_numeric_string(in_str)
    assert out_str == '"1"'

    in_str = "1.1"
    out_str = quote_numeric_string(in_str)
    assert out_str == '"1.1"'

    in_str = "1.1.1"
    out_str = quote_numeric_string(in_str)
    assert out_str == "1.1.1"
